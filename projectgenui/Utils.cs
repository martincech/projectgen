﻿namespace projectgenui
{
    using MahApps.Metro.Controls.Dialogs;
    using System.Threading.Tasks;
    using Veit.TruckProjects;

    static class Utils
    {
        public static async Task UpdateAndStoreUserSettingsAsync(UserSettings us, LoginDialogData result)
        {
            us.Username = result.Username;
            us.Password = result.Password;
            if (result.ShouldRemember)
                await UserSettings.SaveAsync(us);
        }

        public static async Task<UserSettings> LoadUserSettingsAsync()
        {
            return await UserSettings.LoadAsync();
        }

        public static async Task<Services> CreateAndTestServicesAsync(ProjectConfiguration config, UserSettings us)
        {
            return await Services.CreateAndTestServicesAsync(config, us.Username, us.Password);
        }

        public static async Task<Services> CreateAndTestServicesAsync(ProjectConfiguration config, LoginDialogData result)
        {
            return await Services.CreateAndTestServicesAsync(config, result.Username, result.Password);
        }
    }
}
