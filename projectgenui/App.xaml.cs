﻿namespace projectgenui
{
    using NLog;
    using NLog.LayoutRenderers;
    using System.Windows;
    using MahApps.Metro.Controls;
    using MahApps.Metro.Controls.Dialogs;

    public partial class App : Application
    {
        static readonly ILogger logger = LogManager.GetCurrentClassLogger();

        static App()
        {
            LayoutRenderer.Register<DemystifiedExceptionLayoutRenderer>("exception");
        }

        void AppDispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            logger.Fatal(e.Exception, string.Empty);
            (Current.MainWindow as MetroWindow).ShowMessageAsync("Fatal Error", "Ooops, something went wrong, developers are informed...");
        }
    }
}
