﻿namespace projectgenui
{
    using MahApps.Metro.Controls;
    using MahApps.Metro.Controls.Dialogs;
    using System.Threading.Tasks;
    using System.Windows;
    using Veit.TruckProjects;
    using System.Linq;
    using System.Windows.Controls;
    using System;
    using Atlassian.Jira;
    using System.Collections.Generic;

    public partial class MainWindow : MetroWindow
    {
        const string AuthenticationDialogHeader = "JIRA & Confluence Authentication";

        Services services;
        ProjectConfiguration projects;
        JiraUser reporter;

        public MainWindow()
        {
            InitializeComponent();
        }

        async Task<LoginDialogData> ShowLoginDialogAsync(UserSettings us)
        {

            return await this.ShowLoginAsync(
                AuthenticationDialogHeader,
                "Enter your credentials",
                new LoginDialogSettings
                {
                    ColorScheme = MetroDialogOptions.ColorScheme,
                    EnablePasswordPreview = us.Password is null,
                    RememberCheckBoxVisibility = Visibility.Visible,

                    DialogResultOnCancel = MessageDialogResult.Canceled,
                    AnimateShow = true,
                    AnimateHide = true,
                    InitialUsername = us.Username is null ? string.Empty : us.Username,
                    InitialPassword = us.Password is null ? string.Empty : us.Password
                });
        }

        async Task<Services> AuthenticateAsync(UserSettings us)
        {
            Services services = null;
            while (services is null)
            {
                var result = await ShowLoginDialogAsync(us);
                if (result is null)
                    Close();
                else
                {
                    // update credentials
                    await Utils.UpdateAndStoreUserSettingsAsync(us, result);
                    services = await Utils.CreateAndTestServicesAsync(projects, us);
                    if (services is null)
                        await this.ShowMessageAsync(AuthenticationDialogHeader, "Attempt Failed", MessageDialogStyle.Affirmative);
                }
            }
            return services;
        }

        async Task UpdateBindingsAsync(Services services)
        {
            // truck definitions
            comboTruckType.ItemsSource = services.GetTruckTypeCustomFieldValues();
            comboCountry.ItemsSource = services.GetCountryCustomFieldValues();
            comboCustomer.ItemsSource = await services.GetCompanyCustomFieldExistingValuesAsync();
            datepickerBase.SelectedDate = DateTime.Now;

            // users
            var users = services.GetActiveVeitUsers();
            comboProjectAssignee.ItemsSource = users;
            comboSalesAssignee.ItemsSource = users;
            comboManufacturingAssignee.ItemsSource = users;
            comboHandoverAssignee.ItemsSource = users;
            comboEngineeringAssignee.ItemsSource = users;
            comboConstructionAssignee.ItemsSource = users;
            comboAdministrationAssignee.ItemsSource = users;

            // TODO: rework hard-coded values to constants
            comboTruckType.SelectedIndex = 0;
            comboProjectAssignee.SelectedItem = users.FirstOrDefault(u => u.Email == projects.AssigneesByRole["project"]);
            comboSalesAssignee.SelectedItem = users.FirstOrDefault(u => u.Email == projects.AssigneesByRole["sales"]);
            comboConstructionAssignee.SelectedItem = users.FirstOrDefault(u => u.Email == projects.AssigneesByRole["construction"]);
            comboEngineeringAssignee.SelectedItem = users.FirstOrDefault(u => u.Email == projects.AssigneesByRole["engineering"]);
            comboManufacturingAssignee.SelectedItem = users.FirstOrDefault(u => u.Email == projects.AssigneesByRole["manufacturing"]);
            comboHandoverAssignee.SelectedItem = users.FirstOrDefault(u => u.Email == projects.AssigneesByRole["handover"]);
            comboAdministrationAssignee.SelectedItem = users.FirstOrDefault(u => u.Email == projects.AssigneesByRole["administration"]);
        }

        async Task SyncWithJira()
        {
            var controller = await this.ShowProgressAsync("VEIT JIRA", "Syncing data with Jira");
            controller.SetIndeterminate();

            _ = await services.GetCompanyCustomFieldExistingValuesAsync();
            _ = services.GetCountryCustomFieldValues();
            _ = services.GetTruckTypeCustomFieldValues();
            _ = services.GetActiveVeitUsers();

            await controller.CloseAsync();
            await UpdateBindingsAsync(services);
        }

        async void OnWindowLoaded(object sender, RoutedEventArgs e)
        {
            // TODO: load from file
            projects = DefaultProjectConfiguration.Create();
            await projects.SaveAsync("trucks.json");
            var p1 = await ProjectConfiguration.LoadFromFileAsync("trucks.json");

            var us = await Utils.LoadUserSettingsAsync();
            services = await Utils.CreateAndTestServicesAsync(projects, us);
            if (services is null)
                services = await AuthenticateAsync(us);

            reporter = await services.JiraClient.Users.GetUserAsync(Services.GetUsername(us.Username));

            // temporary
            //await GenerateOldProjectWikiPages();

            await SyncWithJira();
        }

        //async Task GenerateOldProjectWikiPages()
        //{
        //    var gc = new GeneratorConfiguration
        //    {
        //        ProjectName = "PL-Cedrob VEIT26 31118",
        //        TruckId = "31118",
        //        ProjectConfiguration = new ProjectConfiguration
        //        {
        //            Atlassian = new AtlassianDefinitions
        //            {
        //                JiraUri = "https://veit.atlassian.net",
        //                JiraId = Guid.Parse("e748a6ee-863e-3d3c-945b-a4f3ec26cf0a"),
        //                ConfluenceUri = "https://veit.atlassian.net/wiki",
        //                ConfluenceParentPageId = 205043411 //57147406
        //            }
        //        }
        //    };
        //    var epic = await services.JiraClient.Issues.GetIssueAsync("TC-117");
        //    await Generator.GenerateConfluencePagesAsync(services, epic, gc);
        //}

        bool ShouldEnableButtonGenerate()
        {
            return
                comboTruckType.SelectedItem != null &&
                !string.IsNullOrWhiteSpace(textboxTruckId.Text) &&
                !string.IsNullOrWhiteSpace(textboxAbraId.Text) &&
                !string.IsNullOrWhiteSpace(comboCustomer.Text) &&
                datepickerBase.SelectedDate.HasValue &&
                datepickerBase.SelectedDate.Value > DateTime.Now.AddDays(-1) &&
                comboProjectAssignee.SelectedItem != null &&
                comboSalesAssignee.SelectedItem != null &&
                comboConstructionAssignee.SelectedItem != null &&
                comboEngineeringAssignee.SelectedItem != null &&
                comboManufacturingAssignee.SelectedItem != null &&
                comboHandoverAssignee.SelectedItem != null &&
                comboAdministrationAssignee.SelectedItem != null;
        }

        string GetProjectName()
        {
            var country = comboCountry.SelectedItem?.ToString();
            var truckType = comboTruckType.SelectedItem?.ToString();
            return Helpers.CreateTruckName(
                country,
                comboCustomer.Text,
                textboxTruckId.Text,
                truckType
            );
        }

        void UpdateUiStates()
        {

            labelTruckDescriptionHeader.Header = GetProjectName();
            buttonGenerate.IsEnabled = ShouldEnableButtonGenerate();
        }

        void ComboSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateUiStates();
            if (sender.Equals(comboTruckType))
            {
                var project = projects.TruckProjects.Where(tp => tp.TruckTypeKeys.Contains(comboTruckType.SelectedItem.ToString())).First();
                var issues = projects.Issues.Where(i => project.Issues.Any(ik => ik.IssueKey == i.Key)).Select(kv => kv.Value);
                listboxTasks.ItemsSource = issues;
            }
        }

        void TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateUiStates();
        }

        async void ButtonGenerateClick(object sender, RoutedEventArgs e)
        {
            var gc = new GeneratorConfiguration
            {
                ProjectConfiguration = projects,
                ProjectName = GetProjectName(),
                TruckType = comboTruckType.SelectedItem?.ToString(),
                Country = comboCountry.SelectedItem?.ToString(),
                TruckId = textboxTruckId.Text,
                AbraId = textboxAbraId.Text,
                Company = comboCustomer.Text,
                Reporter = reporter,
                BaseDate = datepickerBase.SelectedDate.Value,
                Users = new Dictionary<string, JiraUser>
                {
                    ["project"] = comboProjectAssignee.SelectedItem as JiraUser,
                    ["sales"] = comboSalesAssignee.SelectedItem as JiraUser,
                    ["construction"] = comboConstructionAssignee.SelectedItem as JiraUser,
                    ["engineering"] = comboEngineeringAssignee.SelectedItem as JiraUser,
                    ["manufacturing"] = comboManufacturingAssignee.SelectedItem as JiraUser,
                    ["handover"] = comboHandoverAssignee.SelectedItem as JiraUser,
                    ["administration"] = comboAdministrationAssignee.SelectedItem as JiraUser
                }
            };

            var controller = await this.ShowProgressAsync($"Generating Project: {gc.ProjectName}", "Preparing...");
            controller.SetIndeterminate();
            var url = await Generator.GenerateProjectAsync(services, gc, (string s) => controller.SetMessage(s));
            await controller.CloseAsync();
            if (url != null)
            {
                await this.ShowMessageAsync($"Project {gc.ProjectName} generated", url);
                // TODO: clear UI
            }
        }
    }
}
