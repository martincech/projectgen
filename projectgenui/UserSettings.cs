﻿namespace projectgenui
{
    using System;
    using System.Reflection;
    using System.Security.Cryptography;
    using System.Text;
    using Newtonsoft.Json;
    using System.IO;
    using System.Threading.Tasks;

    class UserSettings
    {
        static readonly string filePath = Path.Combine(
            Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
            Assembly.GetExecutingAssembly().GetName().Name,
            "config.json"
        );

        [JsonProperty(PropertyName = "username", Required = Required.AllowNull)]
        public string Username { get; set; }

        [JsonProperty(PropertyName = "password", Required = Required.AllowNull)]
        byte[] EncryptedPassword { get; set; }

        [JsonIgnore]
        public string Password
        {
            get
            {
                if (EncryptedPassword is null)
                    return null;
                var data = ProtectedData.Unprotect(EncryptedPassword, null, DataProtectionScope.CurrentUser);
                return Encoding.UTF8.GetString(data);
            }
            set
            {
                if (value is null)
                    EncryptedPassword = null;
                EncryptedPassword = ProtectedData.Protect(Encoding.UTF8.GetBytes(value), null, DataProtectionScope.CurrentUser);
            }
        }

        static void EnforceDirectory()
        {
            // not yet async version
            Directory.CreateDirectory(Path.GetDirectoryName(filePath));
        }

        public static async Task<UserSettings> LoadAsync()
        {
            EnforceDirectory();
            if (!File.Exists(filePath))
                return new UserSettings();

            var json = await File.ReadAllTextAsync(filePath, Encoding.UTF8);
            return JsonConvert.DeserializeObject<UserSettings>(json);
        }

        public static async Task SaveAsync(UserSettings settings)
        {
            EnforceDirectory();
            var json = JsonConvert.SerializeObject(settings);
            await File.WriteAllTextAsync(filePath, json, Encoding.UTF8);
        }
    }
}
