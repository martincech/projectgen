﻿namespace Veit.TruckProjects
{
    using Atlassian.Jira;
    using Dapplo.Confluence;
    using Dapplo.Confluence.Query;
    using NLog;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class GeneratorConfiguration
    {
        public ProjectConfiguration ProjectConfiguration { get; set; }

        public string ProjectName { get; set; }

        public DateTime BaseDate { get; set; }

        public string AbraId { get; set; }

        public string TruckId { get; set; }

        public string Country { get; set; }

        public string Company { get; set; }

        public string TruckType { get; set; }

        public JiraUser Reporter { get; set; }

        public Dictionary<string, JiraUser> Users { get; set; }
    }

    public static class Generator
    {
        static readonly ILogger logger = LogManager.GetCurrentClassLogger();

        public static async Task<Issue> CreateEpicAsync(Jira jira, GeneratorConfiguration configuration)
        {
            var epic = jira.CreateIssue("TC");
            epic.Type = "Epic";
            epic.CustomFields.Add("Epic Name", configuration.ProjectName);
            epic.Summary = configuration.ProjectName;
            epic.Description = configuration.ProjectName;
            epic.Assignee = configuration.Users["project"].Username;
            epic.Reporter = configuration.Reporter.Username;
            if (!string.IsNullOrWhiteSpace(configuration.Country))
                epic.CustomFields.Add("Country", configuration.Country);
            epic.CustomFields.Add("Truck Type", configuration.TruckType);
            epic.CustomFields.Add("Company", configuration.Company);
            epic.CustomFields.Add("Truck ID", configuration.TruckId);
            epic.CustomFields.Add("ABRA Object ID", configuration.AbraId);
            return await epic.SaveChangesAsync();
        }

        static async Task<Issue> CreateIssueAsync(Jira jira, Issue epic, GeneratorConfiguration conf, Dictionary<string, object> variables, IssueDefinition id, int daysShift)
        {
            var issue = jira.CreateIssue(conf.ProjectConfiguration.Projects[id.ProjectKey]);
            issue.Type = conf.ProjectConfiguration.IssueTypes[id.IssueTypeKey];
            issue.Reporter = conf.Reporter.Username;
            issue.CustomFields.Add("Epic Link", epic.Key.Value);
            issue.DueDate = conf.BaseDate.AddDays(daysShift);
            issue.Assignee = conf.Users[id.AssigneeKey].Username;
            issue.Summary = VariablesExpander.Expand(id.Summary, variables);
            issue.Description = VariablesExpander.Expand(id.Description, variables);
            if (id.Components != null)
                foreach (var c in id.Components)
                    issue.Components.Add(c);

            return await issue.SaveChangesAsync();
        }

        static async Task LinkIssues(Issue from, Issue to, string linkType)
        {
            await from.LinkToIssueAsync(to.Key.Value, linkType);
        }

        public static async Task<string> GenerateProjectAsync(Services services, GeneratorConfiguration config, Action<string> progressCallback)
        {
            Issue epic = null;
            var issues = new Dictionary<string, Issue>();
            try
            {
                var variables = new Dictionary<string, object>
                {
                    ["projectName"] = config.ProjectName,
                    ["truckId"] = config.TruckId,
                    ["truckType"] = config.TruckType,
                    ["abraId"] = config.AbraId
                };

                progressCallback?.Invoke($"Creating: {config.ProjectName}");
                epic = await CreateEpicAsync(services.JiraClient, config);
                var project = config.ProjectConfiguration.TruckProjects.Where(tp => tp.TruckTypeKeys.Contains(config.TruckType)).First();
                foreach (var i in project.Issues)
                {
                    issues.Add(i.IssueKey, await CreateIssueAsync(services.JiraClient, epic, config, variables, config.ProjectConfiguration.Issues[i.IssueKey], i.DaysOffset));
                }
                foreach (var ld in config.ProjectConfiguration.Links)
                {
                    progressCallback?.Invoke($"Linking issues...");
                    if (issues.ContainsKey(ld.FromIssueKey) && issues.ContainsKey(ld.ToIssueKey))
                    {
                        await LinkIssues(issues[ld.FromIssueKey], issues[ld.ToIssueKey], ld.Relation);
                    }
                }
                progressCallback?.Invoke($"Creating Confluence pages...");
                return await GenerateConfluencePagesAsync(services, epic, config);
            }
            catch (Exception e)
            {
                logger.Error(e, string.Empty);

                // delete all issues
                foreach (var i in issues)
                    await services.JiraClient.Issues.DeleteIssueAsync(i.Key);

                if (epic != null)
                        await services.JiraClient.Issues.DeleteIssueAsync(epic.Key.Value);

                return null;
            }
        }

        // TODO: make private again
        public static async Task<string> GenerateConfluencePagesAsync(Services services, Issue epic, GeneratorConfiguration config)
        {
            var content = string.Format(Content, config.ProjectConfiguration.Atlassian.JiraId, epic.Key.Value);

            var truckPage = await services.ConfluenceClient.Content.CreateAsync(ContentTypes.Page, config.ProjectName, "TPP", content, config.ProjectConfiguration.Atlassian.ConfluenceParentPageId);
            _ = await services.ConfluenceClient.Content.CreateAsync(ContentTypes.Page, $"Poznámky k výrobě ({config.TruckId})", "TPP", "", truckPage.Id);
            _ = await services.ConfluenceClient.Content.CreateAsync(ContentTypes.Page, $"Feedback ({config.TruckId})", "TPP", "", truckPage.Id);
            return services.ConfluenceClient.CreateWebUiUri(truckPage.Links).AbsoluteUri;
        }

        const string Content =
@"<ac:layout>
    <ac:layout-section ac:type=""single"">
        <ac:layout-cell>
            <p class=""auto-cursor-target"">
                <br />
            </p>
            <ac:structured-macro ac:name=""info"" ac:schema-version=""1"" ac:macro-id=""641c4005-1f55-4741-a391-aec3bf069d3b"">
                <ac:rich-text-body>
                    <p>
                        <ac:structured-macro ac:name=""jira"" ac:schema-version=""1"" ac:macro-id=""383110cc-51e6-480b-b8d4-e484a3c69bb4"">
                            <ac:parameter ac:name=""server"">System JIRA</ac:parameter>
                            <ac:parameter ac:name=""columns"">key,summary,type,created,updated,due,assignee,reporter,priority,status,resolution</ac:parameter>
                            <ac:parameter ac:name=""serverId"">{0}</ac:parameter>
                            <ac:parameter ac:name=""key"">{1}</ac:parameter>
                        </ac:structured-macro>
                    </p>
                </ac:rich-text-body>
            </ac:structured-macro>
            <p class=""auto-cursor-target"">
                <br />
            </p>
        </ac:layout-cell>
    </ac:layout-section>
    <ac:layout-section ac:type=""two_left_sidebar"">
        <ac:layout-cell>
            <p class=""auto-cursor-target"">
                <br />
            </p>
            <ac:structured-macro ac:name=""panel"" ac:schema-version=""1"" ac:macro-id=""a7f3021a-46f1-4f76-b11a-e08fdc68f3bb"">
                <ac:parameter ac:name=""borderColor"">black</ac:parameter>
                <ac:parameter ac:name=""title"">Odkazy</ac:parameter>
                <ac:rich-text-body>
                    <p>
                        <ac:structured-macro ac:name=""children"" ac:schema-version=""2"" ac:macro-id=""128e8979-a0ec-429b-9340-471896c9bdc5"" />
                    </p>
                </ac:rich-text-body>
            </ac:structured-macro>
            <h2>&Uacute;koly po term&iacute;nu</h2>
            <p>
                <ac:structured-macro ac:name=""jira"" ac:schema-version=""1"" ac:macro-id=""5f29dd57-f7d7-4ccb-9d0e-344023716974"">
                    <ac:parameter ac:name=""server"">System JIRA</ac:parameter>
                    <ac:parameter ac:name=""columns"">key,summary,due,assignee,priority</ac:parameter>
                    <ac:parameter ac:name=""maximumIssues"">30</ac:parameter>
                    <ac:parameter ac:name=""jqlQuery"">&quot;Epic Link&quot; = {1}  AND duedate &lt; now() AND status not in (Closed, Done, Resolved) ORDER BY duedate ASC</ac:parameter>
                    <ac:parameter ac:name=""serverId"">{0}</ac:parameter>
                </ac:structured-macro>
            </p>
        </ac:layout-cell>
        <ac:layout-cell>
            <p>
                <ac:structured-macro ac:name=""jira"" ac:schema-version=""1"" ac:macro-id=""00597d8c-058e-4343-a45a-50958f67a10c"">
                    <ac:parameter ac:name=""server"">System JIRA</ac:parameter>
                    <ac:parameter ac:name=""columns"">key,summary,type,due,assignee,priority,status,resolution</ac:parameter>
                    <ac:parameter ac:name=""maximumIssues"">40</ac:parameter>
                    <ac:parameter ac:name=""jqlQuery"">&quot;Epic Link&quot; = {1}  ORDER BY duedate ASC</ac:parameter>
                    <ac:parameter ac:name= ""serverId"">{0}</ac:parameter>
                </ac:structured-macro>
            </p>
        </ac:layout-cell>
    </ac:layout-section>
</ac:layout>";
    }
}
