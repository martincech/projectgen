﻿namespace Veit.TruckProjects
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;

    [DebuggerDisplay("{OriginalValue}")]
    class Variable : IEquatable<Variable>
    {
        public string Name { get; set; }

        public Type Type { get; set; }

        public string OriginalValue { get; set; }

        public int Padding { get; set; }

        public string FormatString { get; set; }

        public bool Equals(Variable other)
        {
            return
                Name == other.Name &&
                Type == other.Type &&
                OriginalValue == other.OriginalValue &&
                Padding == other.Padding &&
                FormatString == other.FormatString;
        }
    }

    static class VariablesExpander
    {
        static readonly Regex simpleVarRegex = new Regex(@"\{([0-9a-zA-Z]+)\}", RegexOptions.Compiled);
        static readonly Regex paddingVarRegex = new Regex(@"\{([0-9a-zA-Z]+)\,(\-{0,1}[0-9]+)\}", RegexOptions.Compiled);
        static readonly Regex formatterVarRegex = new Regex(@"\{([0-9a-zA-Z]+)\:([\w\d\s\.\,\-_\:\/]+)\}", RegexOptions.Compiled);
        static readonly Regex paddingFormatterVarRegex = new Regex(@"\{([0-9a-zA-Z]+)\,(\-{0,1}[0-9]+)\:([\w\d\s\.\,\-_\:]+)\}", RegexOptions.Compiled);

        static Type GetNamedVariableType(string variableName)
        {
            if (variableName == "date")
                return typeof(DateTime);
            return typeof(string);
        }

        static void ParseSimpleVars(string text, List<Variable> result)
        {
            var matches = simpleVarRegex.Matches(text);
            if (matches.Count > 0)
            {
                foreach (var match in matches)
                {
                    var name = ((Match)match).Groups[1].Value;
                    result.Add(new Variable { Name = name, Type = GetNamedVariableType(name), OriginalValue = ((Match)match).Value });
                }
            }
        }

        static void ParsePaddingVars(string text, List<Variable> result)
        {
            var matches = paddingVarRegex.Matches(text);
            if (matches.Count > 0)
            {
                foreach (var match in matches)
                {
                    var name = ((Match)match).Groups[1].Value;
                    var pad = int.Parse(((Match)match).Groups[2].Value);
                    result.Add(new Variable { Name = name, Padding = pad, Type = GetNamedVariableType(name), OriginalValue = ((Match)match).Value });
                }
            }
        }

        static void ParseFormatterVars(string text, List<Variable> result)
        {
            var matches = formatterVarRegex.Matches(text);
            if (matches.Count > 0)
            {
                foreach (var match in matches)
                {
                    var name = ((Match)match).Groups[1].Value;
                    var format = ((Match)match).Groups[2].Value;
                    result.Add(new Variable { Name = name, FormatString = format, Type = GetNamedVariableType(name), OriginalValue = ((Match)match).Value });
                }
            }
        }

        static void ParsePaddingFormatterVars(string text, List<Variable> result)
        {
            var matches = paddingFormatterVarRegex.Matches(text);
            if (matches.Count > 0)
            {
                foreach (var match in matches)
                {
                    var name = ((Match)match).Groups[1].Value;
                    var pad = int.Parse(((Match)match).Groups[2].Value);
                    var format = ((Match)match).Groups[3].Value;
                    result.Add(new Variable { Name = name, Padding = pad, FormatString = format, Type = GetNamedVariableType(name), OriginalValue = ((Match)match).Value });
                }
            }
        }

        public static List<Variable> Parse(string text)
        {
            var result = new List<Variable>();
            ParseSimpleVars(text, result);
            ParsePaddingVars(text, result);
            ParseFormatterVars(text, result);
            ParsePaddingFormatterVars(text, result);
            return result;
        }

        public static HashSet<string> GetUniqueNames(this List<Variable> vars)
        {
            return new HashSet<string>(vars.Select(v => v.Name).Distinct());
        }

        public static void CheckVariables(HashSet<string> names, Dictionary<string, object> values)
        {
            if (!names.All(n => values.ContainsKey(n)))
                throw new InvalidOperationException("Not all variables defined");
        }

        public static string Expand(string text, Dictionary<string, object> values)
        {
            var variables = Parse(text);
            CheckVariables(variables.GetUniqueNames(), values);

            var sb = new StringBuilder(text);
            variables.ForEach(v => Expand(sb, v, values));
            return sb.ToString();
        }

        static void Expand(StringBuilder sb, Variable v, Dictionary<string, object> values)
        {
            var formatStr = GetFormatString(v);
            var value = string.Format(CultureInfo.InvariantCulture, formatStr, values.First(kv => kv.Key == v.Name).Value);
            sb.Replace(v.OriginalValue, value);
        }

        static string GetFormatString(Variable v)
        {
            var sb = new StringBuilder();
            sb.Append("{0");
            if (v.Padding != 0)
                sb.Append(',').Append(v.Padding);
            if (v.FormatString != null)
                sb.Append(':').Append(v.FormatString);
            sb.Append('}');
            return sb.ToString();
        }
    }
}
