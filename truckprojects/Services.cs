﻿namespace Veit.TruckProjects
{
    using Atlassian.Jira;
    using Dapplo.Confluence;
    using NLog;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    public class Services
    {
        static readonly ILogger logger = LogManager.GetCurrentClassLogger();
        const string TruckCardProjectKey = "TC";
        const string TruckCardIssueKey = "TC-1";

        private Services(string jiraUrl, string confluenceUrl, string username, string secret)
        {
            JiraClient = Jira.CreateRestClient(jiraUrl, username, secret);
            ConfluenceClient = Dapplo.Confluence.ConfluenceClient.Create(new Uri(confluenceUrl));
            ConfluenceClient.SetBasicAuthentication(username, secret);
        }

        public static string GetUsername(string value)
        {
            return value.Split('@')[0];
        }

        public static async Task<Services> CreateAndTestServicesAsync(ProjectConfiguration config, string username, string secret)
        {
            var services = new Services(config.Atlassian.JiraUri, config.Atlassian.ConfluenceUri, username, secret);

            try
            {
                // test it
                var user = Services.GetUsername(username);
                var jUser = await services.JiraClient.Users.GetUserAsync(user);
                //var cUser = await services.ConfluenceClient.User.GetUserAsync(user);
            }
            catch (Exception e)
            {
                logger.Warn(e, string.Empty);
                return null;
            }

            return services;
        }

        public Jira JiraClient { get; private set; }

        public IConfluenceClient ConfluenceClient { get; private set; }

        const int JiraPageItems = 100;
        public async Task QueryJira(string jqlQuery, Action<Issue> issueAction)
        {
            var pos = 0;
            var issuesPage = await JiraClient.Issues.GetIssuesFromJqlAsync(jqlQuery, JiraPageItems, pos);
            while (pos + issuesPage.Count() <= issuesPage.TotalItems)
            {
                foreach (var issue in issuesPage)
                {
                    issueAction(issue);
                }

                pos += issuesPage.ItemsPerPage;
                issuesPage = await JiraClient.Issues.GetIssuesFromJqlAsync(jqlQuery, JiraPageItems, pos);
            }
        }

        List<JiraUser> users = new List<JiraUser>();
        readonly object usersLock = new object();
        public IEnumerable<JiraUser> GetActiveVeitUsers()
        {
            lock (usersLock)
            {
                if (users.Count == 0)
                    users = JiraClient.Users.SearchUsersAsync("").Result
                        .Where(u => !string.IsNullOrWhiteSpace(u.Email) && u.Email.EndsWith("@veit.cz"))
                        .OrderBy(u => u.DisplayName)
                        .ToList();

                return users;
            }
        }

        List<string> truckTypes = new List<string>();
        readonly object truckTypesLock = new object();
        public IEnumerable<string> GetTruckTypeCustomFieldValues()
        {
            lock (truckTypesLock)
            {
                if (truckTypes.Count == 0)
                    truckTypes = JiraClient.Issues.GetFieldsEditMetadataAsync(TruckCardIssueKey).Result
                        .Values
                        .Where(v => v.Name == "Truck Type")
                        .First()
                        .AllowedValues
                        .Select(v => v.Value<string>("value"))
                        .Where(v => v != "Unknown")
                        .ToList();

                return truckTypes;
            }
        }

        List<string> countries = new List<string>();
        readonly object countriesLock = new object();
        public IEnumerable<string> GetCountryCustomFieldValues()
        {
            lock (countriesLock)
            {
                if (countries.Count == 0)
                    countries = JiraClient.Issues.GetFieldsEditMetadataAsync(TruckCardIssueKey).Result
                        .Values
                        .Where(v => v.Name == "Country")
                        .First()
                        .AllowedValues
                        .Select(v => v.Value<string>("value"))
                        .ToList();

                return countries;
            }
        }

        List<string> companies = new List<string>();
        static readonly SemaphoreSlim companiesLock = new SemaphoreSlim(1, 1);
        public async Task<IEnumerable<string>> GetCompanyCustomFieldExistingValuesAsync()
        {
            companiesLock.Wait();
            try
            {
                if (companies.Count == 0)
                {
                    await QueryJira($"project={TruckCardProjectKey}", (i) => companies.Add(i.CustomFields["Company"]?.Values?[0]));
                    companies = companies.Where(c => !string.IsNullOrWhiteSpace(c)).Distinct().OrderBy(c => c).ToList();
                }
                return companies;
            }
            finally
            {
                companiesLock.Release();
            }
        }
    }
}
