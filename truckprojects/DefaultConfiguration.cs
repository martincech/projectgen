﻿namespace Veit.TruckProjects
{
    using System;
    using System.Collections.Generic;

    public static class DefaultProjectConfiguration
    {
        static Dictionary<string, string> GetAssigneesByRole() => new Dictionary<string, string>
        {
            ["project"] = "milan.horak@veit.cz",
            ["sales"] = "svatopluk.zak@veit.cz",
            ["construction"] = "petr.vass@veit.cz",
            ["engineering"] = "david.makovsky@veit.cz",
            ["manufacturing"] = "pavel.nespor@veit.cz",
            ["handover"] = "svatopluk.zak@veit.cz",
            ["administration"] = "klara.vlaskova@veit.cz"
        };

        static Dictionary<string, string> GetIssueTypes() => new Dictionary<string, string>
        {
            ["epic"] = "Epic",
            ["task"] = "Task",
            ["adm"] = "Administrativa",
            ["construction"] = "Konstrukce",
            ["man"] = "Výroba"
        };

        static Dictionary<string, string> GetProjectKeys() => new Dictionary<string, string>
        {
            ["cards"] = "TC",
            ["default"] = "TPP",
            ["eng"] = "ENG",
            ["adm"] = "ADM"
        };

        static Dictionary<string, IssueDefinition> GetIssues() => new Dictionary<string, IssueDefinition>
        {
            ["clarifyDocs"] = new IssueDefinition
            {
                ProjectKey = "default",
                AssigneeKey = "sales",
                IssueTypeKey = "task",
                Summary = "Finish the production handover documents",
                Description = "Finish the production handover documents. Dokončit všechny dokumenty potřebné pro výrobu.",
                Components = null
            },
            ["unapprovedOpts"] = new IssueDefinition
            {
                ProjectKey = "default",
                AssigneeKey = "manufacturing",
                IssueTypeKey = "task",
                Summary = "Write list of unapproved options",
                Description = "Write list of unapproved options.",
                Components = null
            },
            ["chasisOrder"] = new IssueDefinition
            {
                ProjectKey = "default",
                AssigneeKey = "project",
                IssueTypeKey = "task",
                Summary = "Order the chassis",
                Description = "Order the chassis. Objednat podvozek.",
                Components = null
            },
            ["engineOrder"] = new IssueDefinition
            {
                ProjectKey = "default",
                AssigneeKey = "manufacturing",
                IssueTypeKey = "task",
                Summary = "Order the engine",
                Description = "Order the engine. Objednat motor.",
                Components = null
            },
            ["platformOrder"] = new IssueDefinition
            {
                ProjectKey = "default",
                AssigneeKey = "project",
                IssueTypeKey = "task",
                Summary = "Order the lifting platform",
                Description = "Order the lifting platform. Objednat plošinu.",
                Components = null
            },
            ["spec"] = new IssueDefinition
            {
                ProjectKey = "default",
                AssigneeKey = "manufacturing",
                IssueTypeKey = "task",
                Summary = "Create specification",
                Description = "Create specification needed for production, construction and administration.",
                Components = null
            },
            ["samples"] = new IssueDefinition
            {
                ProjectKey = "default",
                AssigneeKey = "sales",
                IssueTypeKey = "task",
                Summary = "Deliver all needed samples from customer",
                Description = "Organize the transport of all needed samples from the customer. Zajistit dodání všech potřebných vzorků od zákazníka.",
                Components = null
            },
            ["trolleySpec"] = new IssueDefinition
            {
                ProjectKey = "default",
                AssigneeKey = "construction",
                IssueTypeKey = "construction",
                Summary = "Drawings of the trolleys",
                Description = "Drawings of the trolleys. Nakreslit výkresy vozíků.",
                Components = null
            },
            ["trolleyOrder"] = new IssueDefinition
            {
                ProjectKey = "default",
                AssigneeKey = "manufacturing",
                IssueTypeKey = "task",
                Summary = "Order trolleys",
                Description = "Order trolleys. Objednat vozíky.",
                Components = null
            },
            ["bbDocCreate"] = new IssueDefinition
            {
                ProjectKey = "default",
                AssigneeKey = "construction",
                IssueTypeKey = "construction",
                Summary = "Production documentation for bodybuilder - create",
                Description = "Create production documentation for bodybuilder. Vytvořit výrobní dokumentaci pro nástavbáře.",
                Components = null
            },
            ["bbDocSend"] = new IssueDefinition
            {
                ProjectKey = "default",
                AssigneeKey = "project",
                IssueTypeKey = "task",
                Summary = "Production documentation for bodybuilder - send",
                Description = "Send production documentation for bodybuilder. Odeslat výrobní dokumentaci pro nástavbáře.",
                Components = null
            },
            ["bbDocCheck"] = new IssueDefinition
            {
                ProjectKey = "default",
                AssigneeKey = "project",
                IssueTypeKey = "construction",
                Summary = "Production documentation from bodybuilder - check",
                Description = "Check production documentation from bodybuilder.",
                Components = null
            },
            ["constructionSpec"] = new IssueDefinition
            {
                ProjectKey = "default",
                AssigneeKey = "construction",
                IssueTypeKey = "construction",
                Summary = "Production documentation - construction",
                Description = "Finish the manufacturing documentation. Dokončit veškerou výrobní dokumentaci.",
                Components = null
            },
            ["engSpec"] = new IssueDefinition
            {
                ProjectKey = "eng",
                AssigneeKey = "engineering",
                IssueTypeKey = "task",
                // TODO: solve
                Summary = "Truck schema: {projectName}",
                Description = "Truck schema.",
                Components = new string[] { "Truck Schema" }
            },
            ["orderMaterial"] = new IssueDefinition
            {
                ProjectKey = "default",
                AssigneeKey = "manufacturing",
                IssueTypeKey = "task",
                Summary = "Orders of the material",
                Description = "Order all needed materials - construction. Objednat všechen materiál - zámečníci.",
                Components = null
            },
            ["bodyTransportOrder"] = new IssueDefinition
            {
                ProjectKey = "adm",
                AssigneeKey = "administration",
                IssueTypeKey = "adm",
                Summary = "Order the transport of the body",
                Description = "Order the transport of the body.",
                Components = null
            },
            ["bodyDelivery"] = new IssueDefinition
            {
                ProjectKey = "default",
                AssigneeKey = "manufacturing",
                IssueTypeKey = "task",
                Summary = "Delivery of the body",
                Description = "Delivery of the body confirmation. Potvrdit že skříň byla dodána k nám.",
                Components = null
            },
            ["platformMounting"] = new IssueDefinition
            {
                ProjectKey = "default",
                AssigneeKey = "project",
                IssueTypeKey = "task",
                Summary = "Mounting of the lifting platform",
                Description = "Mounting of the lifting platform.",
                Components = null
            },
            ["dekra"] = new IssueDefinition
            {
                ProjectKey = "adm",
                AssigneeKey = "administration",
                IssueTypeKey = "adm",
                Summary = "Arrange DEKRA",
                Description = "Arrange DEKRA.",
                Components = null
            },
            ["finishProduction"] = new IssueDefinition
            {
                ProjectKey = "default",
                AssigneeKey = "manufacturing",
                IssueTypeKey = "man",
                Summary = "Finish production",
                Description = "Kompletně dokončená výroba. Auto kompletně otestované. Auto zkontrolované podle obědnávky. Veškerá dokumentace předána.",
                Components = null
            },
            ["presaleService"] = new IssueDefinition
            {
                ProjectKey = "default",
                AssigneeKey = "manufacturing",
                IssueTypeKey = "task",
                Summary = "Do pre-sales service",
                Description = "Domluvit a přistavit auto k předprodejnímu servisu. \n\nV autě má být z výroby: \n\n* Dofukovací sada\n* Zvedák\n* Nářadí\n\nNa předprodejním servisu v Brně se přidá:\n\n* Žárovky\n* Výstražný trojúhelník\n* Lékárnička\n* Hasící přístroj\n",
                Components = null
            },
            ["invoiceAddParts"] = new IssueDefinition
            {
                ProjectKey = "default",
                AssigneeKey = "project",
                IssueTypeKey = "task",
                Summary = "List of additional invoice parts",
                Description = "Create list of additional invoice parts.",
                Components = null
            },
            ["handoverPrep"] = new IssueDefinition
            {
                ProjectKey = "adm",
                AssigneeKey = "administration",
                IssueTypeKey = "adm",
                Summary = "Prepare handover",
                Description = "Prepare the handover of the product (all documents etc.) - připravit vše na předání.",
                Components = null
            },
            ["handover"] = new IssueDefinition
            {
                ProjectKey = "default",
                AssigneeKey = "handover",
                IssueTypeKey = "task",
                Summary = "Handover to the customer",
                Description = "Handover to the customer.",
                Components = null
            },
            ["feedback"] = new IssueDefinition
            {
                ProjectKey = "default",
                AssigneeKey = "project",
                IssueTypeKey = "task",
                Summary = "Share feedback",
                Description = "Share feedback.",
                Components = null
            },
            ["costs"] = new IssueDefinition
            {
                ProjectKey = "default",
                AssigneeKey = "project",
                IssueTypeKey = "task",
                Summary = "Calculate production costs",
                Description = "Calculate production costs.",
                Components = null
            }
        };

        static List<TruckProject> GetTtruckProjects() => new List<TruckProject>
        {
            new TruckProject
            {
                TruckTypeKeys = new string[] { "VEIT3,5UL" },
                Issues = new List<IssueShiftOffset>
                {
                    new IssueShiftOffset { IssueKey = "clarifyDocs", DaysOffset = -154 },
                    new IssueShiftOffset { IssueKey = "unapprovedOpts", DaysOffset = -147 },
                    new IssueShiftOffset { IssueKey = "chasisOrder", DaysOffset = -147 },
                    new IssueShiftOffset { IssueKey = "spec", DaysOffset = -140 },
                    new IssueShiftOffset { IssueKey = "samples", DaysOffset = -112 },
                    new IssueShiftOffset { IssueKey = "trolleySpec", DaysOffset = -98 },
                    new IssueShiftOffset { IssueKey = "trolleyOrder", DaysOffset = -91 },
                    new IssueShiftOffset { IssueKey = "bbDocSend", DaysOffset = -77 },
                    new IssueShiftOffset { IssueKey = "constructionSpec", DaysOffset = -70 },
                    new IssueShiftOffset { IssueKey = "engSpec", DaysOffset = -70 },
                    new IssueShiftOffset { IssueKey = "orderMaterial", DaysOffset = -63 },
                    new IssueShiftOffset { IssueKey = "bodyDelivery", DaysOffset = -35 },
                    new IssueShiftOffset { IssueKey = "finishProduction", DaysOffset = 0 },
                    new IssueShiftOffset { IssueKey = "presaleService", DaysOffset = 3 },
                    new IssueShiftOffset { IssueKey = "invoiceAddParts", DaysOffset = 4 },
                    new IssueShiftOffset { IssueKey = "handoverPrep", DaysOffset = 7 },
                    new IssueShiftOffset { IssueKey = "costs", DaysOffset = 7 },
                    new IssueShiftOffset { IssueKey = "handover", DaysOffset = 14 },
                    new IssueShiftOffset { IssueKey = "feedback", DaysOffset = 18 }
                }
            },
            new TruckProject
            {
                TruckTypeKeys = new string[] { "VEIT5", "VEIT7,5", "VEIT12", "VEIT18", "VEIT26", "VEIT26pup", "VEIT40" },
                Issues = new List<IssueShiftOffset>
                {
                    new IssueShiftOffset { IssueKey = "clarifyDocs", DaysOffset = -154 },
                    new IssueShiftOffset { IssueKey = "unapprovedOpts", DaysOffset = -147 },
                    new IssueShiftOffset { IssueKey = "spec", DaysOffset = -147 },
                    new IssueShiftOffset { IssueKey = "chasisOrder", DaysOffset = -140 },
                    new IssueShiftOffset { IssueKey = "engineOrder", DaysOffset = -112 },
                    new IssueShiftOffset { IssueKey = "samples", DaysOffset = -112 },
                    new IssueShiftOffset { IssueKey = "platformOrder", DaysOffset = -98 },
                    new IssueShiftOffset { IssueKey = "trolleySpec", DaysOffset = -98 },
                    new IssueShiftOffset { IssueKey = "trolleyOrder", DaysOffset = -91 },
                    new IssueShiftOffset { IssueKey = "bbDocCreate", DaysOffset = -84 },
                    new IssueShiftOffset { IssueKey = "bbDocSend", DaysOffset = -77 },
                    new IssueShiftOffset { IssueKey = "constructionSpec", DaysOffset = -70 },
                    new IssueShiftOffset { IssueKey = "engSpec", DaysOffset = -70 },
                    new IssueShiftOffset { IssueKey = "bbDocCheck", DaysOffset = -63 },
                    new IssueShiftOffset { IssueKey = "orderMaterial", DaysOffset = -63 },
                    new IssueShiftOffset { IssueKey = "bodyTransportOrder", DaysOffset = -42 },
                    new IssueShiftOffset { IssueKey = "platformMounting", DaysOffset = -42 },
                    new IssueShiftOffset { IssueKey = "bodyDelivery", DaysOffset = -35 },
                    new IssueShiftOffset { IssueKey = "dekra", DaysOffset = -21 },
                    new IssueShiftOffset { IssueKey = "finishProduction", DaysOffset = 0 },
                    new IssueShiftOffset { IssueKey = "costs", DaysOffset = 7 },
                    new IssueShiftOffset { IssueKey = "handoverPrep", DaysOffset = 21 },
                    new IssueShiftOffset { IssueKey = "handover", DaysOffset = 28 },
                    new IssueShiftOffset { IssueKey = "feedback", DaysOffset = 32 }
                }
            }
        };

        static List<LinkDefinition> GetLinks() => new List<LinkDefinition>
        {
            // Blocks
            new LinkDefinition { FromIssueKey = "chasisOrder", ToIssueKey = "clarifyDocs", Relation = "Blocks" },
            new LinkDefinition { FromIssueKey = "samples", ToIssueKey = "clarifyDocs", Relation = "Blocks" },
            new LinkDefinition { FromIssueKey = "trolleySpec", ToIssueKey = "spec", Relation = "Blocks" },
            new LinkDefinition { FromIssueKey = "trolleyOrder", ToIssueKey = "trolleySpec", Relation = "Blocks" },
            new LinkDefinition { FromIssueKey = "trolleyOrder", ToIssueKey = "spec", Relation = "Blocks" },
            new LinkDefinition { FromIssueKey = "constructionSpec", ToIssueKey = "samples", Relation = "Blocks" },
            new LinkDefinition { FromIssueKey = "constructionSpec", ToIssueKey = "spec", Relation = "Blocks" },
            new LinkDefinition { FromIssueKey = "engSpec", ToIssueKey = "spec", Relation = "Blocks" },
            new LinkDefinition { FromIssueKey = "orderMaterial", ToIssueKey = "engSpec", Relation = "Blocks" },
            new LinkDefinition { FromIssueKey = "orderMaterial", ToIssueKey = "constructionSpec", Relation = "Blocks" },
            new LinkDefinition { FromIssueKey = "finishProduction", ToIssueKey = "orderMaterial", Relation = "Blocks" },
            new LinkDefinition { FromIssueKey = "finishProduction", ToIssueKey = "bodyDelivery", Relation = "Blocks" },
            new LinkDefinition { FromIssueKey = "presaleService", ToIssueKey = "finishProduction", Relation = "Blocks"},
            new LinkDefinition { FromIssueKey = "invoiceAddParts", ToIssueKey = "finishProduction", Relation = "Blocks" },
            new LinkDefinition { FromIssueKey = "costs", ToIssueKey = "finishProduction", Relation = "Blocks" },
            new LinkDefinition { FromIssueKey = "handoverPrep", ToIssueKey = "finishProduction", Relation = "Blocks" },
            new LinkDefinition { FromIssueKey = "handoverPrep", ToIssueKey = "invoiceAddParts", Relation =  "Blocks" },
            new LinkDefinition { FromIssueKey = "handover", ToIssueKey = "handoverPrep",Relation = "Blocks" },
            new LinkDefinition { FromIssueKey = "feedback", ToIssueKey = "handover", Relation = "Blocks" },

            // Relates
            new LinkDefinition { FromIssueKey = "bbDocCheck", ToIssueKey = "bbDocCreate", Relation = "Relates" },
            new LinkDefinition { FromIssueKey = "bbDocCheck", ToIssueKey = "bbDocSend", Relation = "Relates" }
        };

        public static ProjectConfiguration Create()
        {
            return new ProjectConfiguration
            {
                AssigneesByRole = GetAssigneesByRole(),
                IssueTypes = GetIssueTypes(),
                Projects = GetProjectKeys(),
                Issues = GetIssues(),
                TruckProjects = GetTtruckProjects(),
                Links = GetLinks(),
                Atlassian = new AtlassianDefinitions
                {
                    JiraUri = "https://veit.atlassian.net",
                    JiraId = Guid.Parse("e748a6ee-863e-3d3c-945b-a4f3ec26cf0a"),
                    ConfluenceUri = "https://veit.atlassian.net/wiki",
                    ConfluenceParentPageId = 205043411
                }
            };
        }
    }
}
