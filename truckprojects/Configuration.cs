﻿namespace Veit.TruckProjects
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;

    public class IssueDefinition
    {
        [JsonProperty(PropertyName = "projectKey", Required = Required.Always)]
        public string ProjectKey { get; set; }

        [JsonProperty(PropertyName = "issueKey", Required = Required.Always)]
        public string IssueTypeKey { get; set; }

        [JsonProperty(PropertyName = "assigneeKey", Required = Required.Always)]
        public string AssigneeKey { get; set; }

        [JsonProperty(PropertyName = "summary", Required = Required.Always)]
        public string Summary { get; set; }

        [JsonProperty(PropertyName = "description", Required = Required.Default)]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "components", Required = Required.Default)]
        public string[] Components { get; set; }
    }

    public class IssueShiftOffset
    {
        [JsonProperty(PropertyName = "issueKey", Required = Required.Always)]
        public string IssueKey { get; set; }

        [JsonProperty(PropertyName = "daysOffset", Required = Required.Always)]
        public int DaysOffset { get; set; }
    }

    public class TruckProject
    {
        [JsonProperty(PropertyName = "truckTypeKeys", Required = Required.Always)]
        public string[] TruckTypeKeys { get; set; }

        [JsonProperty(PropertyName = "issues", Required = Required.Always)]
        public List<IssueShiftOffset> Issues { get; set; }
    }

    public class AtlassianDefinitions
    {
        [JsonProperty(PropertyName = "jiraUri", Required = Required.Always)]
        public string JiraUri { get; set; } = "http://is.veit.cz"; // "https://veit.atlassian.net"

        [JsonProperty(PropertyName = "jiraId", Required = Required.Always)]
        public Guid JiraId { get; set; } = Guid.Parse("9507b6b1-fe4b-3dcf-b0dc-811734238c99");

        [JsonProperty(PropertyName = "confluenceUri", Required = Required.Always)]
        public string ConfluenceUri { get; set; } = "http://is.veit.cz:8090";

        [JsonProperty(PropertyName = "confluenceProjectsPageId", Required = Required.Always)]
        public long ConfluenceParentPageId { get; set; }
    }

    public class LinkDefinition
    {
        [JsonProperty(PropertyName = "fromIssueKey", Required = Required.Always)]
        public string FromIssueKey { get; set; }

        [JsonProperty(PropertyName = "toIssueKey", Required = Required.Always)]
        public string ToIssueKey { get; set; }

        [JsonProperty(PropertyName = "relation", Required = Required.Always)]
        public string Relation { get; set; }
    }

    public class ProjectConfiguration
    {
        [JsonProperty(PropertyName = "atlassian", Required = Required.Always)]
        public AtlassianDefinitions Atlassian { get; set; }

        [JsonProperty(PropertyName = "roles", Required = Required.Always)]
        public Dictionary<string, string> AssigneesByRole { get; set; }

        [JsonProperty(PropertyName = "issueTypes", Required = Required.Always)]
        public Dictionary<string, string> IssueTypes { get; set; }

        [JsonProperty(PropertyName = "projects", Required = Required.Always)]
        public Dictionary<string, string> Projects { get; set; }

        [JsonProperty(PropertyName = "issues", Required = Required.Always)]
        public Dictionary<string, IssueDefinition> Issues { get; set; }

        [JsonProperty(PropertyName = "links", Required = Required.Always)]
        public List<LinkDefinition> Links { get; set; }

        [JsonProperty(PropertyName = "truckProjects", Required = Required.Always)]
        public List<TruckProject> TruckProjects { get; set; }

        public async Task SaveAsync(string filename)
        {
            using (var fs = File.Open(filename, FileMode.Create, FileAccess.Write))
            using (var sr = new StreamWriter(fs))
            {
                await sr.WriteAsync(JsonConvert.SerializeObject(this, Formatting.Indented));
            }
        }

        public static async Task<ProjectConfiguration> LoadFromFileAsync(string filename)
        {
            using (var fs = File.OpenRead(filename))
            using (var sr = new StreamReader(fs))
            {
                return JsonConvert.DeserializeObject<ProjectConfiguration>(await sr.ReadToEndAsync());
            }
        }
    }
}
